module github.com/dla-marbach/filedriller

go 1.16

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/gomodule/redigo v1.8.4
	github.com/google/uuid v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/richardlehane/siegfried v1.9.1
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
)
